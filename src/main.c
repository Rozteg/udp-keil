/**
  ******************************************************************************
  * @file    main.c
  * @author  Mischenko Rostislav
  * @version V1.0.0
  * @date    10->November-2014
  * @brief   Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "math.h"
/*Personal includes*/
#include "udp_init.h"
#include "timefunc.h"
#include "usart_irq.h"


/*
  Main program.
*/

int main(void)
{
	struct timepack tp;
	err_t 		err;

	
#ifdef SERIAL_DEBUG
	DebugComPort_Init();
	USARTIrq_Init(); 
#endif

	/* Initilaize Ethernet, the LwIP stack  and UDP*/
	err = udp_Init();
	if (err == ERR_OK)
	{
#ifdef UDP_INIT_DEBUG
		UART_printf("Now UDP client (based on LwIP) is run\n\r");		
#endif
		udp_rcv();
		
		while(1)
		{
				
			if (pack_counter>=PACK_SIZE)
			{
				pack_counter = 0;
				err = udp_snd(megapack, sizeof(megapack));
				memset(megapack, 0, sizeof(megapack));

			}	
				
			if (ETH_CheckFrameReceived())
			{ 
				/* process received ethernet packet */
				LwIP_Pkt_Handle();
			}
			/* handle periodic timers for LwIP */
			LwIP_Periodic_Handle(LocalTime);
						
		}
	}
	else
	{
#ifdef UDP_INIT_DEBUG
		UART_printf("Host is not created!!\r\n");	
#endif		
	}
	 
}

/****END OF FILE****/
