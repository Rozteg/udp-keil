#include "usart_irq.h"
#include "checksum.h"

char		urt_rxbuff [100]; //����� ��� ����� ���������
char		megapack [PACK_SIZE][59]; //����� ��� �������� ����� ��������� �� ������
int		urt_count = -1;	//����, ���������� ��������� ������ ��������� (:) � ������ UART
int		pack_counter = 0; //������� �������� ��������� (��� 10 �������� ���������� �������� �� ������)
int		pack_ready = 0; //���� ���������� ������� �� 10-�� �������

/**
  * @brief  Updates the system local time
  * @param  None
  * @retval None
  */
void USARTIrq_Init(void)
{
	__enable_irq(); //���������� ��������� ����������
	NVIC_EnableIRQ(USART1_IRQn); //�������� ���������� �� UART
	NVIC_SetPriority(USART1_IRQn, 0); //���������� �� UART, ��������� 0, ����� �������
	USART1->CR1 |= USART_CR1_RXNEIE;  //���������� �� ������
}

void USART1_IRQHandler (void)
{
   char uart_data;
   if (USART1->SR & USART_SR_RXNE) //���������, ����� �� ��������� � UART
   {
   
	uart_data = USART1->DR; //��������� �� ��� ������ � ����������...
	if (uart_data == ':') 
		urt_count = 0;
	
	if (urt_count >= 0)	
	{
	
		urt_rxbuff[urt_count] = USART1->DR; //�������� �������� ���� � �����.
		urt_count++; //���������� ������� ������ ������.
		
		if (urt_count > 98) //������ �� ������������
		{
			memset(urt_rxbuff, 0, sizeof(urt_rxbuff));
			urt_count = -1;
		}
			
		if(urt_rxbuff[urt_count-1]=='\n') //���� ������ ������� ������
		{
		
			//memcpy(urt, urt_rxbuff, strlen(urt_rxbuff));  //��� ������
			if ( CheckSumOfIncoming(urt_rxbuff) ) //�������� ����������� �����
			{
				memcpy(megapack[pack_counter], urt_rxbuff, (urt_count-2)); 
			}
			pack_counter++;
			memset(urt_rxbuff, 0, sizeof(urt_rxbuff)); //������� �����
			urt_count = -1; //���������� �������	
			
		}
		
	}
		
   }

}

int PacketReady (void)	//������������� ���� ���������� ������ 
{
	if(pack_counter >= PACK_SIZE)
	{
		pack_ready = 1;
	}
	
	return pack_ready;
}

int PacketReset (void) //���������� ���� ���������� ������ 
{
	pack_ready = 0;
	pack_counter = 0;
	return pack_ready;
}







