#include "timefunc.h"
#include "checksum.h"

#define SYSTEMTICK_PERIOD_MS  10


__IO uint32_t 	LocalTime = 0;


char 		stdmsg [MES_SIZE] = ":0203000000000000000000000000000000000000000100\r\n";//�������� ��� ������������ ���������
char 		controlmsg [MES_SIZE];  // ����������� ���������, ����������� � ������� requestProcessing �� ������� ������������
char 		sendbuff [MES_SIZE];  // �����, ������� ���������� ���
struct timepack handlerTime = {0,0};
short int	controlflag = 0;
int		udpflag = 0;
int		uartflag = 0;


void CheckControlFlag(short int *flag);
/**
  * @brief  Updates the system local time
  * @param  None
  * @retval None
  */
void Time_Update(void) //��� ������� ���������� ������ 10 �����������!
{
	LocalTime += SYSTEMTICK_PERIOD_MS;
}

void UART_20ms(void) //��� ������� ���������� ������ 10 �����������!
{
	uartflag++;
	if (uartflag >= 2) //must be 2 for 20 ms
	{
		CheckControlFlag(&controlflag); //���������, �� ������ �� ����������� ���������
						// � ����������� �� ����� �������� ���� ���������� ���������, ���� �����������
		FillPack(sendbuff, handlerTime.sec, handlerTime.msec); // ����� ��������� � ��������� ����� � ������� ����������� �����
		UART_printf(sendbuff);	
		uartflag = 0;
	}
	

}

//���������� ���������� ���������� ������� 10ms
void SysTick_Handler(void)
{

	UART_20ms();
	
	if (handlerTime.msec < 1000) 
	{

		handlerTime.msec = handlerTime.msec + 10;
				
	}
	else 
	{

		handlerTime.msec = handlerTime.msec + 10;
		handlerTime.sec++;
		handlerTime.msec = handlerTime.msec - 1000;
		Set_UDP_Flag(SET);

	}
	
	Time_Update();
}

void Get_Systick_Time(struct timepack* tm)
{
	tm->sec = handlerTime.sec;
	tm->msec = handlerTime.msec;
}


int Set_UDP_Flag(int flag)
{
	
	if (flag == SET || flag == RESET)
	{
		udpflag = flag;		//set flag;
	}
	
	return udpflag;

}

int Check_UDP_Flag (void)
{
	
	return udpflag;

}

void CheckControlFlag(short int* flag)
{
	if (*flag == 0)
	{
		memcpy(sendbuff, stdmsg, MES_SIZE);
		sendbuff[MES_SIZE - 1] = 0x0;
	}
	else
	{
		memcpy(sendbuff, controlmsg, MES_SIZE);
		sendbuff[MES_SIZE - 2] = '\n'; //was - 1
		sendbuff[MES_SIZE - 3] = '\r'; //was - 2
		sendbuff[MES_SIZE - 1] = 0x0;
		*flag = 0;
	}
}

