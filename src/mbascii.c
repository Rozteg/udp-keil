#include "mbascii.h"


/*
 *	Data definition:
 */



const static char	table[] = {
	'0',	/* 0x0 */
	'1',	/* 0x1 */
	'2',	/* 0x2 */
	'3',	/* 0x3 */
	'4',	/* 0x4 */
	'5',	/* 0x5 */
	'6',	/* 0x6 */
	'7',	/* 0x7 */
	'8',	/* 0x8 */
	'9',	/* 0x9 */
	'A',	/* 0xA */
	'B',	/* 0xB */
	'C',	/* 0xC */
	'D',	/* 0xD */
	'E',	/* 0xE */
	'F',	/* 0xF */
	'\0'	/* End of string */
};




static int toMBnibble(const unsigned char ch)
{
	return ((unsigned int) strchr(table, ch) - ((unsigned int) table));
}


static unsigned char toMBascii(const unsigned int x)
{
	return table[x];
}


static void mkMBascii(struct _mb_ascii_ *mba, struct _mb_nibble_ *nib)
{
	mba->major = toMBascii(nib->major);
	mba->minor = toMBascii(nib->minor);
}


static void mkMBnibble(struct _mb_nibble_ *nib, struct _mb_ascii_ *mba)
{
	nib->major = toMBnibble(mba->major);
	nib->minor = toMBnibble(mba->minor);
}


void setMBascii(struct _mb_nibble_ *src, struct _mb_ascii_ *dest, int length)
{
	while ( length-- ) {

		mkMBascii(dest+length, src);
		//dest++;
		src++;
	}
}



void setMBnibble(struct _mb_ascii_ *src, struct _mb_nibble_ *dest, int length)
{
	while ( length-- ) {

		mkMBnibble(dest+length, src);
		//dest++;
		src++;
	}
}


unsigned char calcMBLCR(unsigned char *buf, unsigned int length)
{
	unsigned char	LCR = 0;

	while ( length-- ) {

		LCR += *buf;
		buf++;
	}

	return (unsigned char) (((unsigned char) 0xFF - LCR) + (unsigned char) 1);
}


unsigned short int getMBLCR(unsigned char *buf, unsigned int length)
{
	unsigned char		LCR;
	struct _mb_ascii_ 	*mba = (struct _mb_ascii_ *) &buf[length - 2];

	mkMBnibble((struct _mb_nibble_ *) &LCR, mba);

	return LCR;
}


