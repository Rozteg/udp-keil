#include "udp_init.h"

#ifdef __LWIP_DEBUG_H__
 //#define UDP_INIT_DEBUG
#endif
struct udp_pcb 		*pcb;
struct udp_pcb 		*pcb_rcv;
//struct pbuf		*incoming;




int udp_Init()
{
	err_t 		err;
	struct ip_addr 	server_ip;
	struct ip_addr 	local_ip;
		
	
	ETH_BSP_Config();
	
	LwIP_Init();
	
	pcb = udp_new();
	pcb_rcv = udp_new();
	if (pcb != NULL)
	{

		IP4_ADDR(&server_ip, DEST_IP_ADDR0, DEST_IP_ADDR1, DEST_IP_ADDR2, DEST_IP_ADDR3);
		IP4_ADDR(&local_ip, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
		
		err = udp_bind(pcb_rcv, &local_ip, LOCAL_UDP_PORT);
		if (err == ERR_OK)
		{	
#ifdef UDP_INIT_DEBUG
			UART_printf("port is %i\r\n", pcb->local_port);		
#endif				
			err = udp_connect(pcb, &server_ip, DEST_UDP_PORT);
#ifdef UDP_INIT_DEBUG
			if (err != ERR_OK)
			{
				UART_printf("udp_connect failed\r\n");
				UART_printf("err = %i\r\n",err);
			}
#endif			
		}
#ifdef UDP_INIT_DEBUG
		else
		{
			UART_printf("udp_bind failed\r\n");
			UART_printf("err = %i\r\n",err);
		}
#endif	
		
	}
	else
	{
		
		err = ERR_VAL;
#ifdef UDP_INIT_DEBUG
		UART_printf("err = %i\r\n",err);
#endif
	}
		
	return err;
}
		
int udp_snd(void * data, int len)
{
	err_t 		err;
	struct pbuf 	*message;
	
	message = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_POOL);	 
	if (message != NULL)
	{	
	
#ifdef UDP_INIT_DEBUG
		UART_printf("\r\nMessage OK!\n\r");
#endif
		/* copy data to pbuf */
		err = pbuf_take(message, (char*)data, len);				
		if (err == ERR_OK)
		{
#ifdef UDP_INIT_DEBUG			
			UART_printf("pbuf_take OK!\r\n");
			UART_printf("ready to send\r\n");
#endif			
			/* send udp data */
			err = udp_send(pcb, message);
#ifdef UDP_INIT_DEBUG
			UART_printf("trying\r\n");
#endif
			if (err == ERR_OK)
			{
#ifdef UDP_INIT_DEBUG				
				UART_printf("Sent!\r\n");
#endif				
				/* free pbuf */
				pbuf_free(message);
			}
#ifdef UDP_INIT_DEBUG			
			else
			{
				UART_printf("udp_send failed\r\n");
				UART_printf("err = %i\r\n",err);
			}
#endif
		}
#ifdef UDP_INIT_DEBUG
		else
		{
			UART_printf("pbuf_take failed\r\n");
			UART_printf("err = %i\r\n",err);
		}
#endif

	}
	else
	{
		err = ERR_VAL;
#ifdef UDP_INIT_DEBUG
		UART_printf("Message Fail\n\r");
		UART_printf("err = %i\r\n",err);
#endif
	}

	
	return err;
}

void udp_rcv(void)
{
	udp_recv(pcb_rcv, udp_receive_callback, NULL);
}

void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, struct ip_addr *addr, u16_t port)
{
#ifdef UDP_INIT_DEBUG
	char receive[100];
#endif
	
	//memcpy(incoming, p, sizeof(struct pbuf));
	memcpy(controlmsg, p->payload, p->tot_len);
	controlflag++;	/*increment message count */

#ifdef UDP_INIT_DEBUG

	memcpy(receive, p->payload, p->tot_len);
	UART_printf("got: %s\r\n", receive);
#endif	
	/* Free receive pbuf */
	pbuf_free(p);
  
	/* free the UDP connection, so we can accept new clients */
	udp_disconnect(upcb);   
}


