#ifndef _TMFUNC_H
#define _TMFUNC_H

#define MES_SIZE  50 // was 49, also worked good

#include "stm32f4xx.h"
#include "udp_init.h"


/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */
struct timepack {
	long int 	sec;
	int		msec;
};

/*
 *	Data declaration
 */
extern __IO uint32_t 	LocalTime;
extern char		controlmsg[MES_SIZE];
extern short int	controlflag;
/*
 *	Function declaration
 */
 
#ifdef __cplusplus
extern "C" {
#endif

	void SysTick_Handler(void);
	void Time_Update(void);
	void Get_Systick_Time(struct timepack* tm);
	int Set_UDP_Flag(int flag);
	int Check_UDP_Flag (void);
	

#ifdef __cplusplus
}
#endif

#endif /* _TMFUNC_H */

