#ifndef _MB_ASCII_H
#define _MB_ASCII_H

#include <stdio.h>
#include <string.h>
#include "stm32f4xx.h"
/*
 *	Macros definition
 */

#define _LS_	':'		/* 0x3A */
#define _CR_	'\r'		/* 0x0D */
#define _LF_	'\n'		/* 0x0A */
#define _PACK_	":%s\r\n"



/*
 *	Type declaration
 */

//#pragma pack(1)



void delay_ms(unsigned int );


struct _mb_nibble_ {
	unsigned int	minor	: 4;
	unsigned int	major	: 4;
};

union _mb_byte_ {
	unsigned char		byte;
	struct _mb_nibble_	nib;
};



struct _mb_ascii_ {
	char	major;
	char	minor;
};

//#pragma pack()

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void setMBascii(struct _mb_nibble_ *, struct _mb_ascii_ *, int);

void setMBnibble(struct _mb_ascii_ *, struct _mb_nibble_ *, int);

unsigned char calcMBLCR(unsigned char *, unsigned int);

unsigned short int getMBLCR(unsigned char *, unsigned int);

#ifdef __cplusplus
}
#endif

#endif /* MB_ASCII_H */
