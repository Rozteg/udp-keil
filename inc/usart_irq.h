#ifndef _USARTIRQ_H
#define _USARTIRQ_H

#include "string.h"
#include "timefunc.h"


#define PACK_SIZE 10 // ���������� ������� � ���������
/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */


/*
 *	Data declaration
 */

extern char		megapack [PACK_SIZE][59];
extern int		pack_counter;

/*
 *	Function declaration
 */
 
#ifdef __cplusplus
extern "C" {
#endif

int PacketReady (void);
int PacketReset (void);
void USARTIrq_Init(void);
void USART1_IRQHandler (void);

#ifdef __cplusplus
}
#endif

#endif /* _TMFUNC_H */
