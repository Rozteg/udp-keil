#ifndef _UDPI_H
#define _UDPI_H

#include "stm32f4x7_eth.h"
#include "stm32f4x7_eth_bsp.h"
#include "udp.h"
#include "serial_debug.h"
#include "stm32f4xx_usart.h"
#include "netconf.h"
#include "timefunc.h"
#include "string.h"
#include "main.h"

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */
struct termpack{

	char		begin1;
	char		begin2;
	char		begin3;
	long int 	sec;
	short int 	msec;
	short int	uplus;
	short int	umin;
	char		type;
	char		enrichment;
	float		source;
	short int	resistor;
	short int	stword;
	char		cs;
	char		carryr;
	char		carryn;	
	
 };

/*
 *	Data declaration
 */


/*
 *	Function declaration
 */
 
#ifdef __cplusplus
extern "C" {
#endif
	int udp_Init(void);
	void udp_rcv(void);
	int udp_snd(void * data, int len);
	void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, struct ip_addr *addr, u16_t port);

#ifdef __cplusplus
}
#endif

#endif /* _UDPI_H */

